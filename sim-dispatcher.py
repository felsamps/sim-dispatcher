from Parameter import *
from CfgParser import *
import sys

if __name__ == '__main__':

    configFileName = ""
    printOnly = False
    cfgBool = False

    for arg in sys.argv[1:]:
        if arg == '--cfg':
            cfgBool = True
        elif arg == '--print-only':
            printOnly = True
        elif cfgBool:
            configFileName = str(arg)
            cfgBool = False

    cfg = CfgParser(configFileName, printOnly)
    sim = cfg.parse()
    sim.run()
