import os
from Logger import *

class Simulation:

    def __init__(self, printOnly):
        self.parameters = {}
        self.execName = ""
        self.execPath = ""
        self.basePath = ""
        self.outputFileConfig = []
        self.outputFilesPath = ""
        self.argsLineConfig = []
        self.listOfExecutions = []
        self.commandsToRun = []
        self.printOnly = printOnly
        self.initialDir = ""
        self.outputFileMode = "NONE"

        if not self.printOnly:
            self.logging = Logger()

    def addParameter(self, parameter):
        self.parameters[parameter.getName()] = parameter

    def setExecName(self, execName):
        self.execName = execName

    def setExecPath(self, execPath):
        self.execPath = execPath

    def setBasePath(self, basePath):
        self.basePath = basePath

    def setArgsOutputToFile(self, config):
        self.outputFileConfig = config.split(' ')
        self.outputFileMode = "ARG"

    def setStdOutputToFile(self, config):
        self.outputFileConfig = config.split(' ')
        self.outputFileMode = "CFG"

    def setOutputFilesPath(self, outputFilesPath):
        self.outputFilesPath = outputFilesPath

    def setArgsLineStr(self, config):
        self.argsLineConfig = config.split(' ')

    def setPrefix(self, paramName, prefix):
        self.parameters[paramName].setPrefix(prefix)

    def setSuffix(self, paramName, suffix):
        self.parameters[paramName].setSuffix(suffix)

    def setAliases(self, paramName, aliasesInStr):
        self.parameters[paramName].setAliases(aliasesInStr)

    def __isParam(self, token):
        if token[0] == '$':
            return token[1:]
        else:
            return None

    def __parseExecutionCommand(self, execution):
        command = ""
        for token in self.argsLineConfig:
            print token

            param =  self.__isParam(token)
            if param == None:
                command += token + " "
            else:
                if param == 'args_output_file':
                    command = command[:-1]
                    command += self.outputFilesPath + self.__parseOutputFileName(execution) + " "
                else:
                    parameter = self.parameters[param]
                    command += parameter.getPrefix() + execution[param] + parameter.getSuffix() + " "
        return command

    def __parseOutputFileName(self, execution):
        fileName = ""
        for token in self.outputFileConfig:
            param = self.__isParam(token)
            if param == None:
                fileName += token
            else:
                fileName += self.parameters[param].getAlias(execution[param])
        return fileName


    def __generateCommandsToRun(self):
        self.__genRec(self.parameters.keys(), 0, {})

        commandPrefix = self.execPath + self.execName + " "
        for execution in self.listOfExecutions:
            command = commandPrefix + self.__parseExecutionCommand(execution)

            if self.outputFileMode == "STD":
                command += " > " + self.outputFilesPath + self.__parseOutputFileName(execution)

            self.commandsToRun.append(command)

    def __genRec(self, params, idx, execution):
        param = self.parameters[params[idx]]
        args = param.getArgs()
        for arg in args:
            execution[param.getName()] = arg
            if idx == len(params)-1:
                newExec = dict(execution)
                self.listOfExecutions.append(newExec)
            else:
                self.__genRec(params, idx+1, execution)

    def run(self):
        self.__generateCommandsToRun()

        if not self.printOnly:

            self.initialDir = os.getcwd()
            os.chdir(self.basePath)

            for command in self.commandsToRun:
                self.logging.log("Executing: " + command)
                os.system(command)

            os.chdir(self.initialDir)

        else:
            self.__printExecutionSummary()

    def __printExecutionSummary(self):
        print self.__str__()

        fpExecutions = open('commands.out', 'w')

        commandsToPrint = [cmd + "\n" for cmd in self.commandsToRun]
        for command in commandsToPrint:
            fpExecutions.writelines(command)
        fpExecutions.close()


    def __str__(self):
        returnable = ""
        returnable += "Execution path: " + self.execPath + "\n"
        returnable += "Execution file name: " + self.execName + "\n"
        returnable += "Base path: " + self.basePath + "\n"
        returnable += "Output file path: " + self.outputFilesPath + "\n"
        returnable += "Output file configuration: " + str(self.outputFileConfig) + "\n"
        returnable += "Arguments configuration: " + str(self.argsLineConfig) + "\n"
        returnable += "Execution parameters: " + "\n"
        for param in self.parameters.keys():
            returnable += str(self.parameters[param])

        returnable += "Total of executions: " + str(len(self.commandsToRun)) + "\n"

        return returnable

    def __repr(self):
        return self.__str__()
