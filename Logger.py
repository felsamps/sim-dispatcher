import datetime

loggingPath = 'logs/'

class Logger:
    def __init__(self):
        now = datetime.datetime.now()
        fileName = str(now.date()).replace('-','') + '-' + str(now.time()).split('.')[0].replace(':','') + '.log'
        self.fp = open(loggingPath + fileName, 'w')


    def log(self, text):
        now = datetime.datetime.now()

        textToLog = str(now.time()) + ': '
        textToLog += text + '\n'

        self.fp.write(textToLog)

if __name__ == '__main__':
    log = Logger()
    log.log('Testando 1...')
    log.log('Testando 2...')
    log.log('Testando 3...')
