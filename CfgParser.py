from Simulation import *
from Parameter import *
import re

class CfgParser:
    def __init__(self, cfgFileName, printOnly):
        self.cfgFp = open(cfgFileName, 'r')
        self.simulation = Simulation(printOnly)

    def parse(self):
        buff = self.cfgFp.readlines()
        paramsMode = False
        prefixesMode = False
        suffixesMode = False
        aliasesMode = False

        for cfgLine in buff:
            cfgLine = self.__removeComment(cfgLine)

            tokens = re.split('\s*=\s*', cfgLine, maxsplit=1)
            print tokens

            m = re.search('\S+', tokens[0])
            if m is not None:
                config = tokens[0][m.start() : m.end()]
            else:
                continue

            #work around - to handle with = character in the middle of the configuration values
            #TODO improve it
            """
            if len(tokens) > 2:
                temp = ''
                for s in tokens[1:]:
                    temp += s + '='
                tokens[1] = temp[:-1]
            """

            if config == 'EXEC_NAME':
                self.simulation.setExecName(tokens[1][:-1])
            elif config == 'EXEC_PATH':
                self.simulation.setExecPath(tokens[1][:-1])
            elif config == 'BASE_PATH':
                self.simulation.setBasePath(tokens[1][:-1])
            elif config == 'STD_OUTPUT_TO_FILE':
                configStr = self.__parseLineWithArgs(tokens[1])
                self.simulation.setStdOutputToFile(configStr)
            elif config == 'ARGS_OUTPUT_FILE':
                configStr = self.__parseLineWithArgs(tokens[1])
                self.simulation.setArgsOutputToFile(configStr)
            elif config == 'OUTPUT_FILES_PATH':
                self.simulation.setOutputFilesPath(tokens[1][:-1])
            elif config == 'ARGS_LINE':
                argsLineStr = self.__parseLineWithArgs(tokens[1])
                self.simulation.setArgsLineStr(argsLineStr)
            elif config == 'BEGIN_PARAMS':
                paramsMode = True
            elif config == 'END_PARAMS':
                paramsMode = False
            elif config == 'BEGIN_ALIASES':
                aliasesMode = True
            elif config == 'END_ALIASES':
                aliasesMode = False
            elif config == 'BEGIN_PREFIXES':
                prefixesMode = True
            elif config ==  'END_PREFIXES':
                prefixesMode = False
            elif config == 'BEGIN_SUFFIXES':
                suffixesMode = True
            elif config == 'END_SUFFIXES':
                suffixesMode = False
            elif paramsMode:
                paramName = tokens[0]
                argsLineStr = tokens[1][:-1]
                param = Parameter(paramName, argsLineStr)
                self.simulation.addParameter(param)
            elif aliasesMode:
                paramName = tokens[0]
                aliasesLineStr = tokens[1][:-1]
                self.simulation.setAliases(paramName, aliasesLineStr)
            elif prefixesMode:
                paramName = tokens[0]
                prefix = tokens[1][:-1]
                self.simulation.setPrefix(paramName, prefix)
            elif suffixesMode:
                paramName = tokens[0]
                suffix = tokens[1][:-1]
                self.simulation.setSuffix(paramName, suffix)
            else:
                continue

        return self.simulation


    def __parseLineWithArgs(self, line):
        print line
        regex = '\".*\"'
        match = re.search(regex, line)
        configArgs = line[match.start()+1 : match.end()-1]
        return configArgs

    def __removeComment(self, line):
        match = re.search('#',line)
        if match is not None:
            return line[:match.start()]
        else:
            return line
