import re

class Parameter:

    def __init__(self, name, argsInStr):
        self.name = name
        self.suffix = ""
        self.prefix = ""
        self.__parseArgsInStr(argsInStr)
        self.aliases = {}

    def __parseArgsInStr(self, argsInStr):
        self.args = re.split('\s*,\s*', argsInStr)

    def setPrefix(self, prefix):
        self.prefix = prefix

    def setSuffix(self, suffix):
        self.suffix = suffix

    def getPrefix(self):
        return self.prefix

    def getSuffix(self):
        return self.suffix

    def getArgs(self):
        return self.args

    def getName(self):
        return self.name

    def setAliases(self, aliasesInStr):
        self.aliases = dict(zip(self.args, re.split('\s*,\s*', aliasesInStr)))

    def getAlias(self, arg):
        if len(self.aliases.keys()) > 0: #if there is the definition of aliases
            return self.aliases[arg]
        else:
            return arg

    def __str__(self):
        returnable = ""
        returnable += self.name + ": (" + self.prefix + ") " + str(self.args) + " - " + str(self.aliases) + " (" + self.suffix + ")\n"
        return returnable

    def __repr(self):
        return self.__str__()
